import { Container } from "@mui/material";
import backgroundImg from "./assets/images/background.jpg";
import Home from "./components/home/Home";
import { useState } from "react";
import Detail from "./components/detail/Detail";
const style = {
  container: {
    backgroundImage: `url(${backgroundImg})`,
    backgroundSize: "cover",
    backgroundRepeat: "no-repeat",
    backgroundPosition: "center bottom",
    minHeight: "100vh",
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
  },
};
function App() {
  const [weatherForecast, setWeatherForecast] = useState("");
  const [homePage, setHomePage] = useState(true);
  const [weatherNext4Days, setWeatherNext4Days] = useState([]);
  return (
    <div style={style.container}>
      <Container>
        {homePage ? (
          <Home
            setHomePage={setHomePage}
            setWeatherForecast={setWeatherForecast}
            setWeatherNext4Days={setWeatherNext4Days}
          />
        ) : (
          <Detail
            setHomePage={setHomePage}
            setWeatherForecast={setWeatherForecast}
            weatherForecast={weatherForecast}
            setWeatherNext4Days={setWeatherNext4Days}
            weatherNext4Days={weatherNext4Days}
          />
        )}
      </Container>
    </div>
  );
}

export default App;
