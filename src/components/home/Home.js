import { Grid, Typography } from "@mui/material";
import i02d from "../../assets/images/02d.svg";

import SearchCity from "../detail/SearchCity";
const style = {
  containerStyle: {
    height: "600px",
    borderRadius: "30px",
    justifyContent: "center",
    alignItems: "center",
    position: "relative",
    backgroundColor: "rgba(255, 255, 255, 0.2)",
    boxShadow: "0px 0px 10px silver",
    color: "#365a7a",
  },
};

function Home({ setWeatherNext4Days, setHomePage, setWeatherForecast }) {
  return (
    <Grid container p={5} style={style.containerStyle}>
      <Grid item xs={3} sm={3} md={3} lg={3}>
        <img src={i02d} alt="home" width="250px" />
      </Grid>
      <Grid item xs={9} sm={9} md={9} lg={9}>
        <Typography variant="h1" fontWeight="bold" noWrap>
          Weather Forecast
        </Typography>
      </Grid>
      <SearchCity
        setWeatherNext4Days={setWeatherNext4Days}
        setHomePage={setHomePage}
        setWeatherForecast={setWeatherForecast}
      />
    </Grid>
  );
}
export default Home;
