import { Grid, InputBase } from "@mui/material";
import { useState } from "react";

const style = {
  containerStyle: {
    borderRadius: "30px",
    justifyContent: "center",
    alignItems: "center",
    position: "relative",
    backgroundColor: "rgba(255, 255, 255, 0.2)",
    boxShadow: "0px 0px 10px silver",
    color: "#365a7a",
  },
  inputStyle: {
    display: "inline-block",
    backgroundColor: "white",
    lineHeight: "120%",
    position: "relative",
    borderRadius: "30px",
    fontSize: "20px",
    boxSizing: "border-box",
    color: "black",
    padding: "10px 20px",
    borderWidth: "2px",
    borderStyle: "solid",
  },
};

function SearchCity({
  setWeatherNext4Days,
  setHomePage,
  setWeatherForecast,
  setIcon,
  icon,
}) {
  const [city, setCity] = useState("");
  const [inputPlaceholder, setInputPlaceholder] = useState("Enter a City ...");
  const API_KEY = "7c001e808613e19a3362cc7c1239cb83";
  const getData = async (paramUrl, paramOptions = {}) => {
    const response = await fetch(paramUrl, paramOptions);
    const responseData = await response.json();
    return responseData;
  };
  const callApiGetWeather = () => {
    getData(
      `https://api.openweathermap.org/data/2.5/weather?q=${city}&appid=${API_KEY}`
    )
      .then((data) => {
        if (data.cod === 200) {
          getWeatherNext4Days(data);
          setWeatherForecast(data);
          setHomePage(false);
          console.log(data);
        } else {
          setCity("");
          setInputPlaceholder("City was not found, try again...");
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };
  const getWeatherNext4Days = (data) => {
    getData(
      `https://api.openweathermap.org/data/2.5/forecast?lat=${data.coord.lat}&lon=${data.coord.lon}&appid=${API_KEY}`
    )
      .then((data) => {
        console.log(data);
        let nextFourDayArray = [];
        const timesCheckPerDay = 8;
        for (
          let i = timesCheckPerDay;
          i < data.list.length;
          i += timesCheckPerDay
        ) {
          nextFourDayArray.push(data.list[i]);
        }
        setWeatherNext4Days(nextFourDayArray);
      })
      .catch((error) => {
        console.log(error);
      });
  };
  const changeInputCity = (event) => {
    setCity(event.target.value);
  };
  const checkEnterKey = (event) => {
    if (event.key === "Enter") {
      callApiGetWeather();
    }
  };
  return (
    <>
      <Grid item xs={12} sm={12} md={12} lg={12}>
        <InputBase
          sx={style.inputStyle}
          placeholder={inputPlaceholder}
          fullWidth
          onChange={changeInputCity}
          value={city}
          onKeyUp={checkEnterKey}
        />
      </Grid>
    </>
  );
}
export default SearchCity;
