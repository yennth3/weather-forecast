import { Grid, Typography } from "@mui/material";

const style = {
  dislay: "flex",
  flexDirection: "column",
  justifyContent: "space-evenly",
  alignItems: "center",
  width: " 180px",
  height: "180px",
  borderRadius: "30%",
  backgroundColor: "rgba(255, 255, 255, 0.3)",
  boxShadow: " 3px 0px 8px silver",
  color: "black",
};
const weekends = [
  "Sunday",
  "Monday",
  "Tuesday",
  "Wednesday",
  "Thursday",
  "Friday",
  "Saturday",
];
function WeatherBox({ weatherForecast, imageObject }) {
  const date = new Date(weatherForecast.dt_txt);
  return (
    <Grid style={style} textAlign="center" p={1}>
      <Typography variant="h6" fontWeight="bold" noWrap component="div">
        {weekends[date.getDay()]}
      </Typography>
      <img
        src={imageObject[weatherForecast.weather[0].icon]}
        alt="sun"
        width="100px"
      />
      <Typography variant="h6" fontWeight="bold">
        {Math.round(weatherForecast.main.temp - 273.15)}&#176;C
      </Typography>
    </Grid>
  );
}
export default WeatherBox;
