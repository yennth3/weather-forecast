import { Grid, Typography } from "@mui/material";
import { useState } from "react";
import WeatherBox from "./WeatherBox";
import SearchCity from "./SearchCity";
import Sun from "../../assets/images/50d.svg";
import i01d from "../../assets/images/01d.svg";
import i01n from "../../assets/images/01n.svg";
import i02d from "../../assets/images/02d.svg";
import i02n from "../../assets/images/02n.svg";
import i03d from "../../assets/images/03d.svg";
import i03n from "../../assets/images/03n.svg";
import i04d from "../../assets/images/04d.svg";
import i04n from "../../assets/images/04n.svg";
import i09d from "../../assets/images/09d.svg";
import i09n from "../../assets/images/09n.svg";
import i10d from "../../assets/images/10d.svg";
import i10n from "../../assets/images/10n.svg";
import i11d from "../../assets/images/11d.svg";
import i11n from "../../assets/images/11n.svg";
import i13d from "../../assets/images/13d.svg";
import i13n from "../../assets/images/13n.svg";
const style = {
  containerStyle: {
    borderRadius: "30px",
    justifyContent: "center",
    alignItems: "center",
    position: "relative",
    backgroundColor: "rgba(255, 255, 255, 0.2)",
    boxShadow: "0px 0px 10px silver",
    color: "#365a7a",
    marginTop: "-20px",
  },
  weatherBoxGroup: {
    width: "850px",
    position: "relative",
    top: "10px",
    display: "flex",
    justifyContent: "space-around",
    alignContent: "stretch",
  },
};
function Detail({
  setWeatherNext4Days,
  weatherForecast,
  setHomePage,
  setWeatherForecast,
  weatherNext4Days,
}) {
  const imageObject = {
    "01d": i01d,
    "01n": i01n,
    "02d": i02d,
    "02n": i02n,
    "03d": i03d,
    "03n": i03n,
    "04d": i04d,
    "04n": i04n,
    "09d": i09d,
    "09n": i09n,
    "10d": i10d,
    "10n": i10n,
    "11d": i11d,
    "11n": i11n,
    "13d": i13d,
    "13n": i13n,
  };

  return (
    <Grid container p={5} style={style.containerStyle}>
      <SearchCity
        setHomePage={setHomePage}
        setWeatherForecast={setWeatherForecast}
        setWeatherNext4Days={setWeatherNext4Days}
      />
      <Grid item xs={6} sm={6} md={6} lg={6} textAlign="center">
        <img
          src={imageObject[weatherForecast.weather[0].icon]}
          alt="iconWeather"
          width="250px"
        />
      </Grid>
      <Grid item xs={6} sm={6} md={6} lg={6}>
        <Grid container>
          <Typography variant="h5" noWrap>
            Today
          </Typography>
        </Grid>
        <Grid container my={2}>
          <Typography variant="h3" fontWeight="bold" noWrap color="black">
            {weatherForecast.name}
          </Typography>
        </Grid>
        <Grid container>
          <Typography variant="h5" noWrap>
            Temperature: {Math.round(weatherForecast.main.temp - 273.15)}&#176;C
          </Typography>
        </Grid>
        <Grid container>
          <Typography variant="h5" noWrap>
            {weatherForecast.weather[0].description}
          </Typography>
        </Grid>
      </Grid>
      <ul style={style.weatherBoxGroup}>
        {weatherNext4Days.map((day, index) => (
          <li key={index} style={{ listStyleType: "none" }}>
            <WeatherBox weatherForecast={day} imageObject={imageObject} />
          </li>
        ))}
      </ul>
    </Grid>
  );
}
export default Detail;
